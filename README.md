> **We are in BETA!**
>
> We are still *very* actively refining the code so it is easy to use and doesn't have any legacy stuff in it.
>
> Please don't ask for any help until we've reached version `0.2.0`. However, you are more than welcome to help out!

Atomar
---

Atomar is an opinionated web-app development framework that follows the Model View Controller design pattern using existing open source technologies.

* **model**: [ReadBeanPHP](http://redbeanphp.com/) is an easy ORM for PHP and on-the-fly relational mapper
* **view**: [Twig](http://twig.sensiolabs.org/) is a flexible, fast, and secure template engine for PHP
* **controller**: Atomar uses a currated set of custom controllers.

###Requirements
* PHP 7.0

##Docs
> We are slowly moving the documentation into the wiki. Please bear with us while we transition from documentation everywhere to just one place.

You can learn more about the system at https://github.com/neutrinog/atomar/wiki.

##CLI
You ask if we have a command line client? Why yes we do.

https://github.com/neutrinog/node-atomar-cli
